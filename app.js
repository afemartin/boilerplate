var express = require('express');
var app = express();

app.set('view engine', 'jade');
app.set('views', 'src/server/views');

app.get('/', function (req, res) {
  res.render('index');
});

app.use('/dist/', express.static('dist'));

app.listen(3000, 'localhost', function () {
  console.log('Boilerplate app listening on port 3000!');
});
